package com.example.fancygamename;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.example.fancygamename.database.DatabaseHelper;
import com.example.fancygamename.database.Score;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * View Scoreboard
 */
public class ScoreboardActivity extends AppCompatActivity {
    private DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scoreboard);
        db = DatabaseHelper.getInstance(this);

        //add score after finished game
        float score = getIntent().getLongExtra("score", 0);
        if (score != 0)
            popup(score);

        showScore();
    }

    /**
     * Display score from database
     */
    private void showScore() {
        int id = 1;

        ListView scoreListView = findViewById(R.id.scoreListView);
        ArrayAdapter<String> adapter;
        List<String> initialList = new ArrayList<>(); //load these
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, initialList);
        scoreListView.setAdapter(adapter);
        List<Score> scores = db.getAllScores();
        Collections.sort(scores);
        for (Score iterator : scores) {
            //format displayed row
            adapter.add(String.format(Locale.getDefault(), "%3d %24s %10s Score: %15.3f", id++, iterator.getNick(), "", iterator.getScore()));
        }
        adapter.notifyDataSetChanged();
    }

    /**
     * Show popup to enter user nickname and add current score to database
     *
     * @param score game score
     */
    private void popup(final float score) {
        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        new AlertDialog.Builder(this)
                .setTitle("Scoreboard")
                .setMessage("Enter your nickname")
                .setView(input)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String text = input.getText().toString();
                        //Add new score
                        db.addScore(new Score(0, text, score));
                        showScore();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }
}
