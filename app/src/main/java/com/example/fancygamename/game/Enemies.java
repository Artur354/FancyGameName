package com.example.fancygamename.game;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.fancygamename.R;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

/**
 * Stores information about all enemies on screen
 *
 * Created by Artur on 2018-02-27.
 */

public class Enemies {
    private int width, height;
    private int screenWidth, screenHeight;
    private Bitmap bitmap;
    private Random rand;
    private LinkedList<Enemy> spriteBounds;


    public Enemies(Resources res, int screenWidth, int screenHeight) {

        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;

        bitmap = BitmapFactory.decodeResource(res, R.drawable.fireball);
        width = bitmap.getWidth() / 2;
        height = bitmap.getHeight() / 2;
        spriteBounds = new LinkedList<>();
        rand = new Random();
    }

    /**
     * Add new enemy at random position
     *
     * @param timer pos y
     */
    public void add(int timer) {
        int x = rand.nextInt(screenWidth);
        spriteBounds.add(new Enemy(new Rect(x - width, -50 - height, x + width, -50 + height), timer));
    }

    /**
     * Draw all enemies on screen
     *
     * @param canvas canvas
     * @param y      pos y
     */
    public void draw(Canvas canvas, int y) {
        Enemy enemy;
        Iterator<Enemy> iterator = spriteBounds.iterator();
        while (iterator.hasNext()) {
            enemy = iterator.next();
            if (enemy.getSpriteBound().top >= screenHeight)
                iterator.remove();
            else {
                enemy.getSpriteBound().set(enemy.getSpriteBound().left, y - 50 - enemy.getTimer() - height, enemy.getSpriteBound().right, y - 50 - enemy.getTimer() + height);
                canvas.drawBitmap(bitmap, null, enemy.getSpriteBound(), null);
            }
        }
    }

    /**
     * Check collision with an object
     *
     * @return return true if collision was detected
     */
    public boolean checkCollision(Player player) {

        for (Enemy iterator : spriteBounds)
            if (player.isCollisionDetected(iterator.getSpriteBound(), bitmap))
                return true;
        return false;
    }

}
