package com.example.fancygamename.game;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;

import com.example.fancygamename.R;

/**
 * Player Class
 *
 * Created by Artur on 2018-02-22.
 */

public class Player {
    private int width, height;
    private Bitmap bitmap;
    private Rect playerBounds;

    public Player(Resources res) {
        bitmap = BitmapFactory.decodeResource(res, R.drawable.ship);
        width = bitmap.getWidth() / 2;
        height = bitmap.getHeight() / 2;
        playerBounds = new Rect();
    }

    /**
     * Check collision with a player
     *
     * @param bounds2 colliding object
     * @param bm2     bitmap
     * @return return true when collision is detected
     */
    boolean isCollisionDetected(Rect bounds2, Bitmap bm2) {
        if (Rect.intersects(playerBounds, bounds2)) {   //check rectangle collision
            Rect collisionBounds = getCollisionBounds(playerBounds, bounds2);
            for (int i = collisionBounds.left; i < collisionBounds.right - 2; i += 3) { //check collision pixel by pixel
                for (int j = collisionBounds.top; j < collisionBounds.bottom - 2; j += 3) {
                    int bitmap1Pixel = bitmap.getPixel(i - playerBounds.left, j - playerBounds.top);
                    int bitmap2Pixel = bm2.getPixel(i - bounds2.left, j - bounds2.top);
                    if (isFilled(bitmap1Pixel) && isFilled(bitmap2Pixel)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Create intersection of two rectangles
     *
     * @param rect1 first rectangle
     * @param rect2 second rectangle
     * @return intersection of two rectangles
     */
    private Rect getCollisionBounds(Rect rect1, Rect rect2) {
        int left = Math.max(rect1.left, rect2.left);
        int top = Math.max(rect1.top, rect2.top);
        int right = Math.min(rect1.right, rect2.right);
        int bottom = Math.min(rect1.bottom, rect2.bottom);
        return new Rect(left, top, right, bottom);
    }

    /**
     * Check if color is not transparent
     *
     * @param pixel pixel color
     * @return return true if not transparent
     */
    private boolean isFilled(int pixel) {
        return Color.alpha(pixel) > 24;
    }

    /**
     * Draw player
     *
     * @param canvas canvas
     * @param x      pos x
     * @param y      pos y
     */
    public void draw(Canvas canvas, int x, int y) {

        playerBounds.set(x - width, y - height, x + width, y + height);
        canvas.drawBitmap(bitmap, null, playerBounds, null);
    }


}
