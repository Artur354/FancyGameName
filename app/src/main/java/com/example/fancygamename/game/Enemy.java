package com.example.fancygamename.game;

import android.graphics.Rect;

/**
 * Enemy Class
 *
 * Created by Artur on 2018-02-28.
 */

public class Enemy {
    private float timer;
    private Rect spriteBound;

    public Enemy(Rect spriteBound, float y) {
        this.timer = y;
        this.spriteBound = spriteBound;
    }

    public Rect getSpriteBound() {
        return spriteBound;
    }

    public int getTimer() {
        return (int) timer;
    }
}
