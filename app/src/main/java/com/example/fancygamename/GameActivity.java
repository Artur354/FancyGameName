package com.example.fancygamename;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.fancygamename.view.GameView;

/**
 * Activity to run game
 */
public class GameActivity extends AppCompatActivity {

    private GameView gameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gameView = new GameView(this);
        setContentView(R.layout.activity_game);
        setContentView(gameView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }
}
