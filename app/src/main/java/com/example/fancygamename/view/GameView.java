package com.example.fancygamename.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.example.fancygamename.R;
import com.example.fancygamename.ScoreboardActivity;
import com.example.fancygamename.game.Enemies;
import com.example.fancygamename.game.Player;

/**
 * Display game on screen
 *
 * Created by Artur on 11.02.18.
 */
public class GameView extends SurfaceView implements Runnable {

    private final static int MAX_FPS = 40; //desired fps
    private final static int FRAME_PERIOD = 1000 / MAX_FPS; // the frame period
    private boolean isRunning = false;
    private int playerX, playerY, screenWidth, screenHeight, timer = 0;
    private float step = 10.0f, backgroundPos = 0.0f;
    private boolean isCollision = false;
    private long startTime, elapsedTime;
    private Bitmap backgroundBitmap;
    private Player player;
    private Thread gameThread;
    private SurfaceHolder holder;
    private Enemies enemies;
    private Context context;

    public GameView(Context context) {
        super(context);
        this.context = context;
        holder = getHolder();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenHeight = size.y;
        screenWidth = size.x;
        playerX = screenWidth / 2;
        playerY = screenHeight - size.y / 8;
        player = new Player(getResources());
        isRunning = true;
        startTime = System.currentTimeMillis();
        enemies = new Enemies(getResources(), screenWidth, screenHeight);
        enemies.add(timer);

        //load background bitmap
        backgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.spacebackground);
    }

    /**
     * Update Game
     */
    public void updateGame() {
        this.isCollision = enemies.checkCollision(player);

        if (!isCollision) {
            timer += step;
            backgroundPos = (backgroundPos + step) % screenHeight;
        } else {
            finish();
        }
        if (timer % 100 == 0)
            enemies.add(timer);
    }

    /**
     * Update player position
     */
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent event) {
        playerX = (int) event.getX();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return true;
    }

    /**
     * Main game loop
     */
    @Override
    public void run() {
        while (isRunning) {
            // check if surface is ready
            if (!holder.getSurface().isValid()) {
                continue;
            }
            long started = System.currentTimeMillis();

            // update
            updateGame();
            // draw
            Canvas canvas = holder.lockCanvas();
            if (canvas != null) {
                render(canvas);
                holder.unlockCanvasAndPost(canvas);
            }

            // wait
            float deltaTime = (System.currentTimeMillis() - started);
            int sleepTime = (int) (FRAME_PERIOD - deltaTime);
            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            while (sleepTime < 0) {
                updateGame();
                sleepTime += FRAME_PERIOD;
            }
        }
    }

    public void resume() {
        isRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void pause() {
        isRunning = false;
        boolean retry = true;
        while (retry) {
            try {
                gameThread.join();
                retry = false;
            } catch (InterruptedException e) {
                // try again shutting down the thread
            }
        }
    }

    /**
     * Render game
     */
    private void render(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        drawBackground(canvas);
        enemies.draw(canvas, timer);
        player.draw(canvas, playerX, playerY);
    }

    /**
     * Draw moving background
     *
     * @param canvas canvas
     */
    private void drawBackground(Canvas canvas) {

        Rect backgroundRect = new Rect(0, (int) backgroundPos, screenWidth, screenHeight + (int) backgroundPos);
        Rect backgroundRect2 = new Rect(0, (int) backgroundPos - screenHeight, screenWidth, (int) backgroundPos);
        canvas.drawBitmap(backgroundBitmap, null, backgroundRect, null);
        canvas.drawBitmap(backgroundBitmap, null, backgroundRect2, null);
    }

    /**
     * Finish game and put achieved score on scoreboard
     */
    private void finish() {
        if (elapsedTime == 0) {
            elapsedTime = System.currentTimeMillis() - startTime;

            Intent intent = new Intent(context, ScoreboardActivity.class);
            intent.putExtra("score", elapsedTime);
            context.startActivity(intent);
            //finish game
            ((Activity) context).finish();
        }
    }
}
