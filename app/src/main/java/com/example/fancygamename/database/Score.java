package com.example.fancygamename.database;

import android.support.annotation.NonNull;

/**
 * Score model
 *
 * Created by Artur on 20.02.18.
 */


public class Score implements Comparable<Score> {

    private int id;
    private String nick;
    private float score;

    public Score() {
    }

    public Score(int id, String nick, float score) {
        this.id = id;
        this.nick = nick;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    /**
     * Compare Score
     */
    @Override
    public int compareTo(@NonNull Score score) {
        if (score.getScore() < this.getScore())
            return -1;
        else if (score.getScore() == this.getScore())
            return 0;
        else
            return 1;
    }
}
