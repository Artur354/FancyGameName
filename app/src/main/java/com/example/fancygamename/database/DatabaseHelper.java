package com.example.fancygamename.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Score database
 * <p>
 * Created by Artur on 20.02.18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myDatabase";
    private static final String TABLE_SCORES = "scores";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "nick";
    private static final String KEY_SCORES = "score";
    private static DatabaseHelper singleton;


    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHelper getInstance(Context context) {
        if (singleton == null)
            singleton = new DatabaseHelper(context);
        return singleton;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_SCORES_TABLE = "CREATE TABLE " + TABLE_SCORES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_SCORES + " TEXT" + ")";
        sqLiteDatabase.execSQL(CREATE_SCORES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORES);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    /**
     * Add new score
     *
     * @param score new score
     */
    public void addScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, score.getNick());
        values.put(KEY_SCORES, score.getScore());

        // Inserting Row
        db.insert(TABLE_SCORES, null, values);
        db.close(); // Closing database connection
    }

    /**
     * Get Score by ID
     *
     * @param id ID
     * @return Score
     */
    public Score getScore(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SCORES, new String[]{KEY_ID,
                        KEY_NAME, KEY_SCORES}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        return new Score(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getFloat(2));
    }

    /**
     * List all scores
     *
     * @return List containing all scores form DB
     */
    public List<Score> getAllScores() {
        List<Score> scoreList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SCORES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Score score = new Score();
                score.setId(Integer.parseInt(cursor.getString(0)));
                score.setNick(cursor.getString(1));
                score.setScore(cursor.getFloat(2));
                // Adding Score to list
                scoreList.add(score);
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return Score list
        return scoreList;
    }

    /**
     * Update Score
     *
     * @param score Updated Score
     * @return ID
     */
    public int updateScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, score.getNick());
        values.put(KEY_SCORES, score.getScore());

        // updating row
        return db.update(TABLE_SCORES, values, KEY_ID + " = ?",
                new String[]{String.valueOf(score.getId())});
    }

    /**
     * Delete Score
     *
     * @param score Score
     */
    public void deleteScore(Score score) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SCORES, KEY_ID + " = ?",
                new String[]{String.valueOf(score.getId())});
        db.close();
    }
}
