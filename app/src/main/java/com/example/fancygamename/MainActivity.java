package com.example.fancygamename;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


/**
 * Main activity
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Start new game
     */
    public void newGame(View view) {
        Button buttonContinue = findViewById(R.id.buttonContinue);
        buttonContinue.setVisibility(View.VISIBLE);
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    /**
     * View Scoreboard
     */
    public void viewScoreboard(View view) {
        Intent intent = new Intent(this, ScoreboardActivity.class);
        startActivity(intent);
    }

    /**
     * Continue game
     */
    public void continueGame(View view) {
        //TODO
    }

}
